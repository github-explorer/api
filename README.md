# Github Explorer - Node Proxy API

## Generic proxy API for exploring github repositories

### Tech Stacks
* Nodejs
* Express
* Axios (for proxy calls)


## Instalation
---

Be sure to have [node](https://nodejs.org/en/), [npm](https://www.npmjs.com/), [yarn](https://yarnpkg.com) installed.
We use yarn (and yarn.lock) as default but you should be able to use npm.
To handle multiple node version using [nvm](https://github.com/creationix/nvm) check the package.json for the current node version or the nvmrc file.


### Setup
* clone the repo

  ```
    git clone git@gitlab.com:github-explorer/api.git
  ```

* Go inside the api

  ```
    cd api
  ```
* Setup environment file
  ```
    cp .env.example .env
  ```

* Install packages

  ```
    yarn
  ```

* Start

  ```
    yarn dev
  ```

Alternatively you can use ```yarn start``` to start on production mode. You are free to modify env variables as per the system the app is going to run on.

---


### Setup [Docker]

You can also use docker to run the API if docker and docker-compose are preinstalled on your PC

After cloning the repository just run
```
  docker-compose up -d
```


---
### Test

```
  yarn test
```

### Improvements
* since all controllers are directly depending on the github service I haven't tested them, but we can add some integration tests for them too
* could you chalk to show some important information and errors
* a more sophosticated cors policy
* rate limitation could be added but looks like I am running out of time already....... ;)