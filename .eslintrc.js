const fs = require('fs');

const prettierOptions = JSON.parse(fs.readFileSync('./.prettierrc', 'utf8'));

module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb-base', 'prettier', 'eslint:recommended'],
  plugins: ['prettier', 'jest'],
  settings: {
    'import/resolver': {
      node: {
        paths: ['./api'],
        moduleDirectory: ['./core', './public', './api'],
      },
      'babel-module': {},
    },
  },
  rules: {
    camelcase: 0,
    'prettier/prettier': ['error', prettierOptions],
    'no-underscore-dangle': 0,
    'consistent-return': 0,
    'no-case-declarations': 0,
    'no-await-in-loop': 0,
    'no-unused-vars': 0,
    'import/prefer-default-export': 0,
    'no-console': 0,
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
      },
    ],
  },
  env: {
    jest: true,
  },
};
