import GithubService from 'api/services/Github';
import appConstants from 'api/utils/constants';
import { internalServerError } from 'api/utils/response';

const { ERROR_MESSAGES, SUCCESS_MESSAGES } = appConstants;

export const search = async (req, res) => {
  const { q, sort, order, page, limit } = req.query;
  const github = new GithubService();

  await github
    .searchRepositories({
      q,
      sort,
      order,
      page,
      limit,
    })
    .then((response) => {
      if (response.data) {
        const { total_count, incomplete_results, items } = response.data;

        return res.status(200).json({
          status: SUCCESS_MESSAGES.SUCCESS,
          message: SUCCESS_MESSAGES.REPOSITORIES_FETCHED,
          data: {
            hasNextPage: !incomplete_results,
            repositories: items,
            sort,
            page,
            limit,
            total: total_count,
          },
        });
      }
    })
    .catch((error) => {
      switch (error.response.status) {
        case 304:
          return res.status(304).json({
            status: ERROR_MESSAGES.ERROR,
            message: ERROR_MESSAGES.DATA_NOT_MODIFIED,
          });
        case 422:
          return res.status(422).json({
            status: ERROR_MESSAGES.ERROR,
            message: ERROR_MESSAGES.UNPROCESSABLE_ENTITY,
          });
        case 503:
          return res.status(503).json({
            status: ERROR_MESSAGES.ERROR,
            message: ERROR_MESSAGES.SERVICE_UNAVAILABLE,
          });
        default:
          internalServerError(res, error);
      }
    });
};
