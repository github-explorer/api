/* eslint-disable no-console */
import applyCommonMiddlewares from 'api/middlewares/common';
import searchRoutes from 'api/routes/search';
import repositoryRoutes from 'api/routes/repository';

const setV1Routes = (app) => {
  applyCommonMiddlewares(app);
  console.log('Setting up routes for api v1');
  //  setup v1 routes here
  app.use('/api/search', searchRoutes);
  app.use('/api/repository', repositoryRoutes);
};

export default setV1Routes;
