import appConstants from 'api/utils/constants';
import { internalServerError } from 'api/utils/response';
import GithubService from 'api/services/Github';

const { ERROR_MESSAGES, SUCCESS_MESSAGES } = appConstants;

export const getRepositoryDetails = async (req, res) => {
  const { repositoryUser, repositoryName } = req.params;

  const github = new GithubService();

  await github
    .getRepositoryDetail(repositoryUser, repositoryName)
    .then((response) => {
      if (response.data) {
        return res.status(200).json({
          status: SUCCESS_MESSAGES.SUCCESS,
          message: SUCCESS_MESSAGES.REPOSITORY_FETCHED,
          repository: response.data,
        });
      }
    })
    .catch((error) => {
      switch (error.response.status) {
        case 403:
          return res.status(403).json({
            status: ERROR_MESSAGES.ERROR,
            message: ERROR_MESSAGES.FORBIDDEN,
          });
        case 404:
          return res.status(404).json({
            status: ERROR_MESSAGES.ERROR,
            message: ERROR_MESSAGES.REPOSITORY_NOT_FOUND,
          });
        default:
          internalServerError(res, error);
      }
    });
};

export const getRepositoryReadme = async (req, res) => {
  const { repositoryUser, repositoryName } = req.params;

  const github = new GithubService();

  await github
    .getRepositoryReadme(repositoryUser, repositoryName)
    .then((response) => {
      if (response.data) {
        return res.status(200).json({
          status: SUCCESS_MESSAGES.SUCCESS,
          message: SUCCESS_MESSAGES.REPOSITORY_README_FETCHED,
          readme: response.data,
        });
      }
    });
};
