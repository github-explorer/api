import axios from 'axios';

require('dotenv').config();

// Repository search endpoint https://docs.github.com/en/rest/reference/search#search-repositories
export const REPOSITORIES_SEARCH_ROUTE = '/search/repositories';
export const REPOSITORY_ROUTE = '/repos';

class Github {
  client = null;

  getClient = () => {
    // This returs a basic axios instance for making the request
    if (!this.client) {
      const newClient = axios.create({
        baseURL: process.env.GITHUB_BASE_API_URL,
      });
      this.client = newClient;
    }

    return this.client;
  };

  /**
   * performs search on github repository api
   * params:
   *  sort: stars, forks, help-wanted-issues, updated
   *  order: desc, asc (default desc)
   *  per_page: max 100
   *  page: 1
   *  q: query string
   */
  searchRepositories = async ({
    limit = 10,
    order = 'desc',
    page = 1,
    q,
    sort,
  }) => {
    const client = this.getClient();
    const searchParams = new URLSearchParams({
      q,
      sort,
      order,
      page,
      per_page: limit,
    }).toString();

    return client.get(`${REPOSITORIES_SEARCH_ROUTE}?${searchParams}`);
  };

  getRepositoryDetail = (repositoryUser, repositoryName) => {
    const client = this.getClient();

    return client.get(
      `${REPOSITORY_ROUTE}/${repositoryUser}/${repositoryName}`,
    );
  };

  getRepositoryReadme = (repositoryUser, repositoryName) => {
    const client = this.getClient();

    return client.get(
      `${REPOSITORY_ROUTE}/${repositoryUser}/${repositoryName}/readme`,
    );
  };
}

export default Github;
