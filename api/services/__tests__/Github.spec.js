import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import GithubService, {
  REPOSITORY_ROUTE,
  REPOSITORIES_SEARCH_ROUTE,
} from '../Github';

const TEST_REPO_NAME = 'Hello-World';
const TEST_REPO_USER_NAME = 'octocat';
const TEST_REPO_FULL_NAME = `${TEST_REPO_USER_NAME}/${TEST_REPO_NAME}`;

describe('github service test', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });

  describe('when get repositories is called', () => {
    it('should return repositories list', async () => {
      // This is going to be a PITA
      const response = {
        status: 'Request success',
        message: 'Repositories fetched',
        data: {
          hasNextPage: true,
          repositories: [
            {
              id: 1045381,
              node_id: 'MDEwOlJlcG9zaXRvcnkxMDQ1Mzgx',
              name: 'phonegap-start',
              full_name: 'phonegap/phonegap-start',
              private: false,
              owner: {
                login: 'phonegap',
                id: 60365,
                node_id: 'MDEyOk9yZ2FuaXphdGlvbjYwMzY1',
                avatar_url: 'https://avatars.githubusercontent.com/u/60365?v=4',
                gravatar_id: '',
                url: 'https://api.github.com/users/phonegap',
                html_url: 'https://github.com/phonegap',
                followers_url:
                  'https://api.github.com/users/phonegap/followers',
                following_url:
                  'https://api.github.com/users/phonegap/following{/other_user}',
                gists_url:
                  'https://api.github.com/users/phonegap/gists{/gist_id}',
                starred_url:
                  'https://api.github.com/users/phonegap/starred{/owner}{/repo}',
                subscriptions_url:
                  'https://api.github.com/users/phonegap/subscriptions',
                organizations_url: 'https://api.github.com/users/phonegap/orgs',
                repos_url: 'https://api.github.com/users/phonegap/repos',
                events_url:
                  'https://api.github.com/users/phonegap/events{/privacy}',
                received_events_url:
                  'https://api.github.com/users/phonegap/received_events',
                type: 'Organization',
                site_admin: false,
              },
              html_url: 'https://github.com/phonegap/phonegap-start',
              description: 'PhoneGap Hello World app',
              fork: false,
              url: 'https://api.github.com/repos/phonegap/phonegap-start',
              forks_url:
                'https://api.github.com/repos/phonegap/phonegap-start/forks',
              keys_url:
                'https://api.github.com/repos/phonegap/phonegap-start/keys{/key_id}',
              collaborators_url:
                'https://api.github.com/repos/phonegap/phonegap-start/collaborators{/collaborator}',
              teams_url:
                'https://api.github.com/repos/phonegap/phonegap-start/teams',
              hooks_url:
                'https://api.github.com/repos/phonegap/phonegap-start/hooks',
              issue_events_url:
                'https://api.github.com/repos/phonegap/phonegap-start/issues/events{/number}',
              events_url:
                'https://api.github.com/repos/phonegap/phonegap-start/events',
              assignees_url:
                'https://api.github.com/repos/phonegap/phonegap-start/assignees{/user}',
              branches_url:
                'https://api.github.com/repos/phonegap/phonegap-start/branches{/branch}',
              tags_url:
                'https://api.github.com/repos/phonegap/phonegap-start/tags',
              blobs_url:
                'https://api.github.com/repos/phonegap/phonegap-start/git/blobs{/sha}',
              git_tags_url:
                'https://api.github.com/repos/phonegap/phonegap-start/git/tags{/sha}',
              git_refs_url:
                'https://api.github.com/repos/phonegap/phonegap-start/git/refs{/sha}',
              trees_url:
                'https://api.github.com/repos/phonegap/phonegap-start/git/trees{/sha}',
              statuses_url:
                'https://api.github.com/repos/phonegap/phonegap-start/statuses/{sha}',
              languages_url:
                'https://api.github.com/repos/phonegap/phonegap-start/languages',
              stargazers_url:
                'https://api.github.com/repos/phonegap/phonegap-start/stargazers',
              contributors_url:
                'https://api.github.com/repos/phonegap/phonegap-start/contributors',
              subscribers_url:
                'https://api.github.com/repos/phonegap/phonegap-start/subscribers',
              subscription_url:
                'https://api.github.com/repos/phonegap/phonegap-start/subscription',
              commits_url:
                'https://api.github.com/repos/phonegap/phonegap-start/commits{/sha}',
              git_commits_url:
                'https://api.github.com/repos/phonegap/phonegap-start/git/commits{/sha}',
              comments_url:
                'https://api.github.com/repos/phonegap/phonegap-start/comments{/number}',
              issue_comment_url:
                'https://api.github.com/repos/phonegap/phonegap-start/issues/comments{/number}',
              contents_url:
                'https://api.github.com/repos/phonegap/phonegap-start/contents/{+path}',
              compare_url:
                'https://api.github.com/repos/phonegap/phonegap-start/compare/{base}...{head}',
              merges_url:
                'https://api.github.com/repos/phonegap/phonegap-start/merges',
              archive_url:
                'https://api.github.com/repos/phonegap/phonegap-start/{archive_format}{/ref}',
              downloads_url:
                'https://api.github.com/repos/phonegap/phonegap-start/downloads',
              issues_url:
                'https://api.github.com/repos/phonegap/phonegap-start/issues{/number}',
              pulls_url:
                'https://api.github.com/repos/phonegap/phonegap-start/pulls{/number}',
              milestones_url:
                'https://api.github.com/repos/phonegap/phonegap-start/milestones{/number}',
              notifications_url:
                'https://api.github.com/repos/phonTEST_REPO_FULL_NAMEegap/phonegap-start/notifications{?since,all,participating}',
              labels_url:
                'https://api.github.com/repos/phonegap/phonegap-start/labels{/name}',
              releases_url:
                'https://api.github.com/repos/phonegap/phonegap-start/releases{/id}',
              deployments_url:
                'https://api.github.com/repos/phonegap/phonegap-start/deployments',
              created_at: '2010-11-02T17:45:54Z',
              updated_at: '2022-01-07T10:55:35Z',
              pushed_at: '2021-12-24T11:16:30Z',
              git_url: 'git://github.com/phonegap/phonegap-start.git',
              ssh_url: 'git@github.com:phonegap/phonegap-start.git',
              clone_url: 'https://github.com/phonegap/phonegap-start.git',
              svn_url: 'https://github.com/phonegap/phonegap-start',
              homepage: '',
              size: 8050,
              stargazers_count: 3427,
              watchers_count: 3427,
              language: 'JavaScript',
              has_issues: false,
              has_projects: false,
              has_downloads: true,
              has_wiki: false,
              has_pages: false,
              forks_count: 6676,
              mirror_url: null,
              archived: false,
              disabled: false,
              open_issues_count: 25,
              license: {
                key: 'other',
                name: 'Other',
                spdx_id: 'NOASSERTION',
                url: null,
                node_id: 'MDc6TGljZW5zZTA=',
              },
              allow_forking: true,
              is_template: false,
              topics: [],
              visibility: 'public',
              forks: 6676,
              open_issues: 25,
              watchers: 3427,
              default_branch: 'master',
              score: 1,
            },
          ],
          total: 2228435,
        },
      };
      const params = {
        limit: 1,
        order: 'desc',
        page: 1,
        q: 'test',
        sort: 'desc',
      };

      const searchParams = new URLSearchParams({
        q: params.q,
        sort: params.sort,
        order: params.order,
        page: params.page,
        per_page: params.limit,
      }).toString();

      mock
        .onGet(`${REPOSITORIES_SEARCH_ROUTE}?${searchParams}`)
        .reply(200, response);
      const github = new GithubService();

      // When
      const result = await github.searchRepositories(params);

      // Then
      expect(mock.history.get[0].url).toEqual(
        `${REPOSITORIES_SEARCH_ROUTE}?${searchParams}`,
      );
      expect(result.data).toEqual(response);
    });

    it('should return repo detail', async () => {
      // This is going to be a PITA
      const response = {
        id: 1296269,
        node_id: 'MDEwOlJlcG9zaXRvcnkxMjk2MjY5',
        name: 'Hello-World',
        full_name: `${TEST_REPO_FULL_NAME}`,
        owner: {
          login: 'octocat',
          id: 1,
          node_id: 'MDQ6VXNlcjE=',
          avatar_url: 'https://github.com/images/error/octocat_happy.gif',
          gravatar_id: '',
          url: 'https://api.github.com/users/octocat',
          html_url: 'https://github.com/octocat',
          followers_url: 'https://api.github.com/users/octocat/followers',
          following_url:
            'https://api.github.com/users/octocat/following{/other_user}',
          gists_url: 'https://api.github.com/users/octocat/gists{/gist_id}',
          starred_url:
            'https://api.github.com/users/octocat/starred{/owner}{/repo}',
          subscriptions_url:
            'https://api.github.com/users/octocat/subscriptions',
          organizations_url: 'https://api.github.com/users/octocat/orgs',
          repos_url: 'https://api.github.com/users/octocat/repos',
          events_url: 'https://api.github.com/users/octocat/events{/privacy}',
          received_events_url:
            'https://api.github.com/users/octocat/received_events',
          type: 'User',
          site_admin: false,
        },
        private: false,
        html_url: 'https://github.com/octocat/Hello-World',
        description: 'This your first repo!',
        fork: false,
        url: 'https://api.github.com/repos/octocat/Hello-World',
        archive_url:
          'https://api.github.com/repos/octocat/Hello-World/{archive_format}{/ref}',
        assignees_url:
          'https://api.github.com/repos/octocat/Hello-World/assignees{/user}',
        blobs_url:
          'https://api.github.com/repos/octocat/Hello-World/git/blobs{/sha}',
        branches_url:
          'https://api.github.com/repos/octocat/Hello-World/branches{/branch}',
        collaborators_url:
          'https://api.github.com/repos/octocat/Hello-World/collaborators{/collaborator}',
        comments_url:
          'https://api.github.com/repos/octocat/Hello-World/comments{/number}',
        commits_url:
          'https://api.github.com/repos/octocat/Hello-World/commits{/sha}',
        compare_url:
          'https://api.github.com/repos/octocat/Hello-World/compare/{base}...{head}',
        contents_url:
          'https://api.github.com/repos/octocat/Hello-World/contents/{+path}',
        contributors_url:
          'https://api.github.com/repos/octocat/Hello-World/contributors',
        deployments_url:
          'https://api.github.com/repos/octocat/Hello-World/deployments',
        downloads_url:
          'https://api.github.com/repos/octocat/Hello-World/downloads',
        events_url: 'https://api.github.com/repos/octocat/Hello-World/events',
        forks_url: 'https://api.github.com/repos/octocat/Hello-World/forks',
        git_commits_url:
          'https://api.github.com/repos/octocat/Hello-World/git/commits{/sha}',
        git_refs_url:
          'https://api.github.com/repos/octocat/Hello-World/git/refs{/sha}',
        git_tags_url:
          'https://api.github.com/repos/octocat/Hello-World/git/tags{/sha}',
        git_url: 'git:github.com/octocat/Hello-World.git',
        issue_comment_url:
          'https://api.github.com/repos/octocat/Hello-World/issues/comments{/number}',
        issue_events_url:
          'https://api.github.com/repos/octocat/Hello-World/issues/events{/number}',
        issues_url:
          'https://api.github.com/repos/octocat/Hello-World/issues{/number}',
        keys_url:
          'https://api.github.com/repos/octocat/Hello-World/keys{/key_id}',
        labels_url:
          'https://api.github.com/repos/octocat/Hello-World/labels{/name}',
        languages_url:
          'https://api.github.com/repos/octocat/Hello-World/languages',
        merges_url: 'https://api.github.com/repos/octocat/Hello-World/merges',
        milestones_url:
          'https://api.github.com/repos/octocat/Hello-World/milestones{/number}',
        notifications_url:
          'https://api.github.com/repos/octocat/Hello-World/notifications{?since,all,participating}',
        pulls_url:
          'https://api.github.com/repos/octocat/Hello-World/pulls{/number}',
        releases_url:
          'https://api.github.com/repos/octocat/Hello-World/releases{/id}',
        ssh_url: 'git@github.com:octocat/Hello-World.git',
        stargazers_url:
          'https://api.github.com/repos/octocat/Hello-World/stargazers',
        statuses_url:
          'https://api.github.com/repos/octocat/Hello-World/statuses/{sha}',
        subscribers_url:
          'https://api.github.com/repos/octocat/Hello-World/subscribers',
        subscription_url:
          'https://api.github.com/repos/octocat/Hello-World/subscription',
        tags_url: 'https://api.github.com/repos/octocat/Hello-World/tags',
        teams_url: 'https://api.github.com/repos/octocat/Hello-World/teams',
        trees_url:
          'https://api.github.com/repos/octocat/Hello-World/git/trees{/sha}',
        clone_url: 'https://github.com/octocat/Hello-World.git',
        mirror_url: 'git:git.example.com/octocat/Hello-World',
        hooks_url: 'https://api.github.com/repos/octocat/Hello-World/hooks',
        svn_url: 'https://svn.github.com/octocat/Hello-World',
        homepage: 'https://github.com',
        language: null,
        forks_count: 9,
        forks: 9,
        stargazers_count: 80,
        watchers_count: 80,
        watchers: 80,
        size: 108,
        default_branch: 'master',
        open_issues_count: 0,
        open_issues: 0,
        is_template: false,
        topics: ['octocat', 'atom', 'electron', 'api'],
        has_issues: true,
        has_projects: true,
        has_wiki: true,
        has_pages: false,
        has_downloads: true,
        archived: false,
        disabled: false,
        visibility: 'public',
        pushed_at: '2011-01-26T19:06:43Z',
        created_at: '2011-01-26T19:01:12Z',
        updated_at: '2011-01-26T19:14:43Z',
        permissions: {
          pull: true,
          push: false,
          admin: false,
        },
        allow_rebase_merge: true,
        template_repository: {
          id: 1296269,
          node_id: 'MDEwOlJlcG9zaXRvcnkxMjk2MjY5',
          name: 'Hello-World-Template',
          full_name: 'octocat/Hello-World-Template',
          owner: {
            login: 'octocat',
            id: 1,
            node_id: 'MDQ6VXNlcjE=',
            avatar_url: 'https://github.com/images/error/octocat_happy.gif',
            gravatar_id: '',
            url: 'https://api.github.com/users/octocat',
            html_url: 'https://github.com/octocat',
            followers_url: 'https://api.github.com/users/octocat/followers',
            following_url:
              'https://api.github.com/users/octocat/following{/other_user}',
            gists_url: 'https://api.github.com/users/octocat/gists{/gist_id}',
            starred_url:
              'https://api.github.com/users/octocat/starred{/owner}{/repo}',
            subscriptions_url:
              'https://api.github.com/users/octocat/subscriptions',
            organizations_url: 'https://api.github.com/users/octocat/orgs',
            repos_url: 'https://api.github.com/users/octocat/repos',
            events_url: 'https://api.github.com/users/octocat/events{/privacy}',
            received_events_url:
              'https://api.github.com/users/octocat/received_events',
            type: 'User',
            site_admin: false,
          },
          private: false,
          html_url: 'https://github.com/octocat/Hello-World-Template',
          description: 'This your first repo!',
          fork: false,
          url: 'https://api.github.com/repos/octocat/Hello-World-Template',
          archive_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/{archive_format}{/ref}',
          assignees_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/assignees{/user}',
          blobs_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/git/blobs{/sha}',
          branches_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/branches{/branch}',
          collaborators_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/collaborators{/collaborator}',
          comments_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/comments{/number}',
          commits_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/commits{/sha}',
          compare_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/compare/{base}...{head}',
          contents_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/contents/{+path}',
          contributors_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/contributors',
          deployments_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/deployments',
          downloads_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/downloads',
          events_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/events',
          forks_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/forks',
          git_commits_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/git/commits{/sha}',
          git_refs_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/git/refs{/sha}',
          git_tags_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/git/tags{/sha}',
          git_url: 'git:github.com/octocat/Hello-World-Template.git',
          issue_comment_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/issues/comments{/number}',
          issue_events_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/issues/events{/number}',
          issues_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/issues{/number}',
          keys_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/keys{/key_id}',
          labels_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/labels{/name}',
          languages_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/languages',
          merges_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/merges',
          milestones_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/milestones{/number}',
          notifications_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/notifications{?since,all,participating}',
          pulls_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/pulls{/number}',
          releases_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/releases{/id}',
          ssh_url: 'git@github.com:octocat/Hello-World-Template.git',
          stargazers_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/stargazers',
          statuses_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/statuses/{sha}',
          subscribers_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/subscribers',
          subscription_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/subscription',
          tags_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/tags',
          teams_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/teams',
          trees_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/git/trees{/sha}',
          clone_url: 'https://github.com/octocat/Hello-World-Template.git',
          mirror_url: 'git:git.example.com/octocat/Hello-World-Template',
          hooks_url:
            'https://api.github.com/repos/octocat/Hello-World-Template/hooks',
          svn_url: 'https://svn.github.com/octocat/Hello-World-Template',
          homepage: 'https://github.com',
          language: null,
          forks: 9,
          forks_count: 9,
          stargazers_count: 80,
          watchers_count: 80,
          watchers: 80,
          size: 108,
          default_branch: 'master',
          open_issues: 0,
          open_issues_count: 0,
          is_template: true,
          license: {
            key: 'mit',
            name: 'MIT License',
            url: 'https://api.github.com/licenses/mit',
            spdx_id: 'MIT',
            node_id: 'MDc6TGljZW5zZW1pdA==',
            html_url: 'https://api.github.com/licenses/mit',
          },
          topics: ['octocat', 'atom', 'electron', 'api'],
          has_issues: true,
          has_projects: true,
          has_wiki: true,
          has_pages: false,
          has_downloads: true,
          archived: false,
          disabled: false,
          visibility: 'public',
          pushed_at: '2011-01-26T19:06:43Z',
          created_at: '2011-01-26T19:01:12Z',
          updated_at: '2011-01-26T19:14:43Z',
          permissions: {
            admin: false,
            push: false,
            pull: true,
          },
          allow_rebase_merge: true,
          temp_clone_token: 'ABTLWHOULUVAXGTRYU7OC2876QJ2O',
          allow_squash_merge: true,
          allow_auto_merge: false,
          delete_branch_on_merge: true,
          allow_merge_commit: true,
          subscribers_count: 42,
          network_count: 0,
        },
        temp_clone_token: 'ABTLWHOULUVAXGTRYU7OC2876QJ2O',
        allow_squash_merge: true,
        allow_auto_merge: false,
        delete_branch_on_merge: true,
        allow_merge_commit: true,
        subscribers_count: 42,
        network_count: 0,
        license: {
          key: 'mit',
          name: 'MIT License',
          spdx_id: 'MIT',
          url: 'https://api.github.com/licenses/mit',
          node_id: 'MDc6TGljZW5zZW1pdA==',
        },
        organization: {
          login: 'octocat',
          id: 1,
          node_id: 'MDQ6VXNlcjE=',
          avatar_url: 'https://github.com/images/error/octocat_happy.gif',
          gravatar_id: '',
          url: 'https://api.github.com/users/octocat',
          html_url: 'https://github.com/octocat',
          followers_url: 'https://api.github.com/users/octocat/followers',
          following_url:
            'https://api.github.com/users/octocat/following{/other_user}',
          gists_url: 'https://api.github.com/users/octocat/gists{/gist_id}',
          starred_url:
            'https://api.github.com/users/octocat/starred{/owner}{/repo}',
          subscriptions_url:
            'https://api.github.com/users/octocat/subscriptions',
          organizations_url: 'https://api.github.com/users/octocat/orgs',
          repos_url: 'https://api.github.com/users/octocat/repos',
          events_url: 'https://api.github.com/users/octocat/events{/privacy}',
          received_events_url:
            'https://api.github.com/users/octocat/received_events',
          type: 'Organization',
          site_admin: false,
        },
        parent: {
          id: 1296269,
          node_id: 'MDEwOlJlcG9zaXRvcnkxMjk2MjY5',
          name: 'Hello-World',
          full_name: 'octocat/Hello-World',
          owner: {
            login: 'octocat',
            id: 1,
            node_id: 'MDQ6VXNlcjE=',
            avatar_url: 'https://github.com/images/error/octocat_happy.gif',
            gravatar_id: '',
            url: 'https://api.github.com/users/octocat',
            html_url: 'https://github.com/octocat',
            followers_url: 'https://api.github.com/users/octocat/followers',
            following_url:
              'https://api.github.com/users/octocat/following{/other_user}',
            gists_url: 'https://api.github.com/users/octocat/gists{/gist_id}',
            starred_url:
              'https://api.github.com/users/octocat/starred{/owner}{/repo}',
            subscriptions_url:
              'https://api.github.com/users/octocat/subscriptions',
            organizations_url: 'https://api.github.com/users/octocat/orgs',
            repos_url: 'https://api.github.com/users/octocat/repos',
            events_url: 'https://api.github.com/users/octocat/events{/privacy}',
            received_events_url:
              'https://api.github.com/users/octocat/received_events',
            type: 'User',
            site_admin: false,
          },
          private: false,
          html_url: 'https://github.com/octocat/Hello-World',
          description: 'This your first repo!',
          fork: false,
          url: 'https://api.github.com/repos/octocat/Hello-World',
          archive_url:
            'https://api.github.com/repos/octocat/Hello-World/{archive_format}{/ref}',
          assignees_url:
            'https://api.github.com/repos/octocat/Hello-World/assignees{/user}',
          blobs_url:
            'https://api.github.com/repos/octocat/Hello-World/git/blobs{/sha}',
          branches_url:
            'https://api.github.com/repos/octocat/Hello-World/branches{/branch}',
          collaborators_url:
            'https://api.github.com/repos/octocat/Hello-World/collaborators{/collaborator}',
          comments_url:
            'https://api.github.com/repos/octocat/Hello-World/comments{/number}',
          commits_url:
            'https://api.github.com/repos/octocat/Hello-World/commits{/sha}',
          compare_url:
            'https://api.github.com/repos/octocat/Hello-World/compare/{base}...{head}',
          contents_url:
            'https://api.github.com/repos/octocat/Hello-World/contents/{+path}',
          contributors_url:
            'https://api.github.com/repos/octocat/Hello-World/contributors',
          deployments_url:
            'https://api.github.com/repos/octocat/Hello-World/deployments',
          downloads_url:
            'https://api.github.com/repos/octocat/Hello-World/downloads',
          events_url: 'https://api.github.com/repos/octocat/Hello-World/events',
          forks_url: 'https://api.github.com/repos/octocat/Hello-World/forks',
          git_commits_url:
            'https://api.github.com/repos/octocat/Hello-World/git/commits{/sha}',
          git_refs_url:
            'https://api.github.com/repos/octocat/Hello-World/git/refs{/sha}',
          git_tags_url:
            'https://api.github.com/repos/octocat/Hello-World/git/tags{/sha}',
          git_url: 'git:github.com/octocat/Hello-World.git',
          issue_comment_url:
            'https://api.github.com/repos/octocat/Hello-World/issues/comments{/number}',
          issue_events_url:
            'https://api.github.com/repos/octocat/Hello-World/issues/events{/number}',
          issues_url:
            'https://api.github.com/repos/octocat/Hello-World/issues{/number}',
          keys_url:
            'https://api.github.com/repos/octocat/Hello-World/keys{/key_id}',
          labels_url:
            'https://api.github.com/repos/octocat/Hello-World/labels{/name}',
          languages_url:
            'https://api.github.com/repos/octocat/Hello-World/languages',
          merges_url: 'https://api.github.com/repos/octocat/Hello-World/merges',
          milestones_url:
            'https://api.github.com/repos/octocat/Hello-World/milestones{/number}',
          notifications_url:
            'https://api.github.com/repos/octocat/Hello-World/notifications{?since,all,participating}',
          pulls_url:
            'https://api.github.com/repos/octocat/Hello-World/pulls{/number}',
          releases_url:
            'https://api.github.com/repos/octocat/Hello-World/releases{/id}',
          ssh_url: 'git@github.com:octocat/Hello-World.git',
          stargazers_url:
            'https://api.github.com/repos/octocat/Hello-World/stargazers',
          statuses_url:
            'https://api.github.com/repos/octocat/Hello-World/statuses/{sha}',
          subscribers_url:
            'https://api.github.com/repos/octocat/Hello-World/subscribers',
          subscription_url:
            'https://api.github.com/repos/octocat/Hello-World/subscription',
          tags_url: 'https://api.github.com/repos/octocat/Hello-World/tags',
          teams_url: 'https://api.github.com/repos/octocat/Hello-World/teams',
          trees_url:
            'https://api.github.com/repos/octocat/Hello-World/git/trees{/sha}',
          clone_url: 'https://github.com/octocat/Hello-World.git',
          mirror_url: 'git:git.example.com/octocat/Hello-World',
          hooks_url: 'https://api.github.com/repos/octocat/Hello-World/hooks',
          svn_url: 'https://svn.github.com/octocat/Hello-World',
          homepage: 'https://github.com',
          language: null,
          forks_count: 9,
          stargazers_count: 80,
          watchers_count: 80,
          size: 108,
          default_branch: 'master',
          open_issues_count: 0,
          is_template: true,
          topics: ['octocat', 'atom', 'electron', 'api'],
          has_issues: true,
          has_projects: true,
          has_wiki: true,
          has_pages: false,
          has_downloads: true,
          archived: false,
          disabled: false,
          visibility: 'public',
          pushed_at: '2011-01-26T19:06:43Z',
          created_at: '2011-01-26T19:01:12Z',
          updated_at: '2011-01-26T19:14:43Z',
          permissions: {
            admin: false,
            push: false,
            pull: true,
          },
          allow_rebase_merge: true,
          temp_clone_token: 'ABTLWHOULUVAXGTRYU7OC2876QJ2O',
          allow_squash_merge: true,
          allow_auto_merge: false,
          delete_branch_on_merge: true,
          allow_merge_commit: true,
          subscribers_count: 42,
          network_count: 0,
          license: {
            key: 'mit',
            name: 'MIT License',
            url: 'https://api.github.com/licenses/mit',
            spdx_id: 'MIT',
            node_id: 'MDc6TGljZW5zZW1pdA==',
            html_url: 'https://api.github.com/licenses/mit',
          },
          forks: 1,
          open_issues: 1,
          watchers: 1,
        },
        source: {
          id: 1296269,
          node_id: 'MDEwOlJlcG9zaXRvcnkxMjk2MjY5',
          name: 'Hello-World',
          full_name: 'octocat/Hello-World',
          owner: {
            login: 'octocat',
            id: 1,
            node_id: 'MDQ6VXNlcjE=',
            avatar_url: 'https://github.com/images/error/octocat_happy.gif',
            gravatar_id: '',
            url: 'https://api.github.com/users/octocat',
            html_url: 'https://github.com/octocat',
            followers_url: 'https://api.github.com/users/octocat/followers',
            following_url:
              'https://api.github.com/users/octocat/following{/other_user}',
            gists_url: 'https://api.github.com/users/octocat/gists{/gist_id}',
            starred_url:
              'https://api.github.com/users/octocat/starred{/owner}{/repo}',
            subscriptions_url:
              'https://api.github.com/users/octocat/subscriptions',
            organizations_url: 'https://api.github.com/users/octocat/orgs',
            repos_url: 'https://api.github.com/users/octocat/repos',
            events_url: 'https://api.github.com/users/octocat/events{/privacy}',
            received_events_url:
              'https://api.github.com/users/octocat/received_events',
            type: 'User',
            site_admin: false,
          },
          private: false,
          html_url: 'https://github.com/octocat/Hello-World',
          description: 'This your first repo!',
          fork: false,
          url: 'https://api.github.com/repos/octocat/Hello-World',
          archive_url:
            'https://api.github.com/repos/octocat/Hello-World/{archive_format}{/ref}',
          assignees_url:
            'https://api.github.com/repos/octocat/Hello-World/assignees{/user}',
          blobs_url:
            'https://api.github.com/repos/octocat/Hello-World/git/blobs{/sha}',
          branches_url:
            'https://api.github.com/repos/octocat/Hello-World/branches{/branch}',
          collaborators_url:
            'https://api.github.com/repos/octocat/Hello-World/collaborators{/collaborator}',
          comments_url:
            'https://api.github.com/repos/octocat/Hello-World/comments{/number}',
          commits_url:
            'https://api.github.com/repos/octocat/Hello-World/commits{/sha}',
          compare_url:
            'https://api.github.com/repos/octocat/Hello-World/compare/{base}...{head}',
          contents_url:
            'https://api.github.com/repos/octocat/Hello-World/contents/{+path}',
          contributors_url:
            'https://api.github.com/repos/octocat/Hello-World/contributors',
          deployments_url:
            'https://api.github.com/repos/octocat/Hello-World/deployments',
          downloads_url:
            'https://api.github.com/repos/octocat/Hello-World/downloads',
          events_url: 'https://api.github.com/repos/octocat/Hello-World/events',
          forks_url: 'https://api.github.com/repos/octocat/Hello-World/forks',
          git_commits_url:
            'https://api.github.com/repos/octocat/Hello-World/git/commits{/sha}',
          git_refs_url:
            'https://api.github.com/repos/octocat/Hello-World/git/refs{/sha}',
          git_tags_url:
            'https://api.github.com/repos/octocat/Hello-World/git/tags{/sha}',
          git_url: 'git:github.com/octocat/Hello-World.git',
          issue_comment_url:
            'https://api.github.com/repos/octocat/Hello-World/issues/comments{/number}',
          issue_events_url:
            'https://api.github.com/repos/octocat/Hello-World/issues/events{/number}',
          issues_url:
            'https://api.github.com/repos/octocat/Hello-World/issues{/number}',
          keys_url:
            'https://api.github.com/repos/octocat/Hello-World/keys{/key_id}',
          labels_url:
            'https://api.github.com/repos/octocat/Hello-World/labels{/name}',
          languages_url:
            'https://api.github.com/repos/octocat/Hello-World/languages',
          merges_url: 'https://api.github.com/repos/octocat/Hello-World/merges',
          milestones_url:
            'https://api.github.com/repos/octocat/Hello-World/milestones{/number}',
          notifications_url:
            'https://api.github.com/repos/octocat/Hello-World/notifications{?since,all,participating}',
          pulls_url:
            'https://api.github.com/repos/octocat/Hello-World/pulls{/number}',
          releases_url:
            'https://api.github.com/repos/octocat/Hello-World/releases{/id}',
          ssh_url: 'git@github.com:octocat/Hello-World.git',
          stargazers_url:
            'https://api.github.com/repos/octocat/Hello-World/stargazers',
          statuses_url:
            'https://api.github.com/repos/octocat/Hello-World/statuses/{sha}',
          subscribers_url:
            'https://api.github.com/repos/octocat/Hello-World/subscribers',
          subscription_url:
            'https://api.github.com/repos/octocat/Hello-World/subscription',
          tags_url: 'https://api.github.com/repos/octocat/Hello-World/tags',
          teams_url: 'https://api.github.com/repos/octocat/Hello-World/teams',
          trees_url:
            'https://api.github.com/repos/octocat/Hello-World/git/trees{/sha}',
          clone_url: 'https://github.com/octocat/Hello-World.git',
          mirror_url: 'git:git.example.com/octocat/Hello-World',
          hooks_url: 'https://api.github.com/repos/octocat/Hello-World/hooks',
          svn_url: 'https://svn.github.com/octocat/Hello-World',
          homepage: 'https://github.com',
          language: null,
          forks_count: 9,
          stargazers_count: 80,
          watchers_count: 80,
          size: 108,
          default_branch: 'master',
          open_issues_count: 0,
          is_template: true,
          topics: ['octocat', 'atom', 'electron', 'api'],
          has_issues: true,
          has_projects: true,
          has_wiki: true,
          has_pages: false,
          has_downloads: true,
          archived: false,
          disabled: false,
          visibility: 'public',
          pushed_at: '2011-01-26T19:06:43Z',
          created_at: '2011-01-26T19:01:12Z',
          updated_at: '2011-01-26T19:14:43Z',
          permissions: {
            admin: false,
            push: false,
            pull: true,
          },
          allow_rebase_merge: true,
          temp_clone_token: 'ABTLWHOULUVAXGTRYU7OC2876QJ2O',
          allow_squash_merge: true,
          allow_auto_merge: false,
          delete_branch_on_merge: true,
          allow_merge_commit: true,
          subscribers_count: 42,
          network_count: 0,
          license: {
            key: 'mit',
            name: 'MIT License',
            url: 'https://api.github.com/licenses/mit',
            spdx_id: 'MIT',
            node_id: 'MDc6TGljZW5zZW1pdA==',
            html_url: 'https://api.github.com/licenses/mit',
          },
          forks: 1,
          open_issues: 1,
          watchers: 1,
        },
      };

      mock
        .onGet(`${REPOSITORY_ROUTE}/${TEST_REPO_USER_NAME}/${TEST_REPO_NAME}`)
        .reply(200, response);
      const github = new GithubService();

      // When
      const result = await github.getRepositoryDetail(
        TEST_REPO_USER_NAME,
        TEST_REPO_NAME,
      );

      // Then
      expect(mock.history.get[0].url).toEqual(
        `${REPOSITORY_ROUTE}/${TEST_REPO_USER_NAME}/${TEST_REPO_NAME}`,
      );
      expect(result.data).toEqual(response);
    });

    it('should return repo readme', async () => {
      // This is going to be a PITA
      const response = {
        type: 'file',
        encoding: 'base64',
        size: 5362,
        name: 'README.md',
        path: 'README.md',
        content: 'encoded content ...',
        sha: '3d21ec53a331a6f037a91c368710b99387d012c1',
        url: 'https://api.github.com/repos/octokit/octokit.rb/contents/README.md',
        git_url:
          'https://api.github.com/repos/octokit/octokit.rb/git/blobs/3d21ec53a331a6f037a91c368710b99387d012c1',
        html_url: 'https://github.com/octokit/octokit.rb/blob/master/README.md',
        download_url:
          'https://raw.githubusercontent.com/octokit/octokit.rb/master/README.md',
        _links: {
          git: 'https://api.github.com/repos/octokit/octokit.rb/git/blobs/3d21ec53a331a6f037a91c368710b99387d012c1',
          self: 'https://api.github.com/repos/octokit/octokit.rb/contents/README.md',
          html: 'https://github.com/octokit/octokit.rb/blob/master/README.md',
        },
      };

      mock
        .onGet(
          `${REPOSITORY_ROUTE}/${TEST_REPO_USER_NAME}/${TEST_REPO_NAME}/readme`,
        )
        .reply(200, response);
      const github = new GithubService();

      // When
      const result = await github.getRepositoryReadme(
        TEST_REPO_USER_NAME,
        TEST_REPO_NAME,
      );

      // Then
      expect(mock.history.get[0].url).toEqual(
        `${REPOSITORY_ROUTE}/${TEST_REPO_USER_NAME}/${TEST_REPO_NAME}/readme`,
      );
      expect(result.data).toEqual(response);
    });
  });
});
