import express from 'express';

import {
  getRepositoryDetails,
  getRepositoryReadme,
} from 'api/controllers/repository';
import { internalServerError } from 'api/utils/response';

const router = express.Router();

router.get('/detail/:repositoryUser/:repositoryName', async (req, res) => {
  try {
    await getRepositoryDetails(req, res);
  } catch (error) {
    internalServerError(res, error);
  }
});

router.get(
  '/detail/:repositoryUser/:repositoryName/readme',
  async (req, res) => {
    try {
      await getRepositoryReadme(req, res);
    } catch (error) {
      internalServerError(res, error);
    }
  },
);

export default router;
