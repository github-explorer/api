import express from 'express';

import { search } from 'api/controllers/search';
import { internalServerError } from 'api/utils/response';

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    await search(req, res);
  } catch (error) {
    internalServerError(res, error);
  }
});

export default router;
