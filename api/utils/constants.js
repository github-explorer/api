const appConstants = {
  ERROR_MESSAGES: {
    FAILED: 'Request failed',
    INTERNAL_SERVER_ERROR: 'Internal Server Error',
    UNPROCESSABLE_ENTITY: 'Unprocessable Entity',
    DATA_NOT_MODIFIED: 'Data not modified',
    SERVICE_UNAVAILABLE: 'Service Unavailable',
    FORBIDDEN: 'Forbidden',
    REPOSITORY_NOT_FOUND: 'Repository not found',
  },
  SUCCESS_MESSAGES: {
    REPOSITORIES_FETCHED: 'Repositories fetched',
    SUCCESS: 'Request success',
    REPOSITORY_FETCHED: 'Repository fetched',
    REPOSITORY_README_FETCHED: 'Repository README Fetched',
  },
};

export default appConstants;
