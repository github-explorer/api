import appConstants from './constants';

const { FAILED, INTERNAL_SERVER_ERROR } = appConstants.ERROR_MESSAGES;

// sends internal server error response
export const internalServerError = (res, error) => {
  console.log(error);
  res.status(500).json({
    status: FAILED,
    message: INTERNAL_SERVER_ERROR,
    error,
  });
};
