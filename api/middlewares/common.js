import express from 'express';
import cors from 'cors';
import compression from 'compression';

require('dotenv').config();

function applyCommonMiddlewares(app) {
  // Basic middlewares that will be applicable for whole api
  const origin =
    process.env.NODE_ENV === 'production'
      ? (receivedOrigin, cb) => {
          const whitelist = process.env.CORS_ORIGIN
            ? process.env.CORS_ORIGIN.split(',')
            : [];
          cb(null, whitelist.includes(receivedOrigin));
        }
      : '*';
  app.use(
    cors({
      origin,
    }),
  );

  app.use(compression());
  app.use(express.urlencoded({ limit: '1mb', extended: true }));
  app.use(express.json({ limit: '1mb' }));
}

export default applyCommonMiddlewares;
