import express from 'express';

import appConfig from 'core/config';
import { initServer } from 'core/server';

require('dotenv').config();

const app = express();

// initialising server
initServer(app, appConfig);

export default app;
