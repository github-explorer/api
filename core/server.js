import express from 'express';
import os from 'os';
import path from 'path';
import helmet from 'helmet';
import morgan from 'morgan';

import setV1Routes from 'api/controllers';

const ifaces = os.networkInterfaces();

const { MAX_REQ_BODY_SIZE, PUBLIC_DIRECTORY } =
  require('./config').EXPRESS_CONFIG;

const _printEnvDetails = (appConfig) => {
  // importing modules
  let _lanIP;

  Object.keys(ifaces).forEach((ifname) => {
    let alias = 0;

    ifaces[ifname].forEach((iface) => {
      if (iface.family !== 'IPv4' || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        // console.log(ifname + ':' + alias, iface.address);
        _lanIP = iface.address;
      } else {
        // this interface has only one ipv4 adress
        // console.log(ifname, iface.address);
        _lanIP = iface.address;
      }

      alias += 1;
    });
  });

  console.log(`App running on ${appConfig.name} environment`);
  console.log(
    `Run app on local machine http://127.0.0.1:${appConfig.app.port}`,
  );

  console.log(
    `Run app over local network http://${_lanIP}:${appConfig.app.port}`,
  );
};

function _configureServer(app) {
  console.log('Configuring server...');

  //  body parser
  app.use(express.json());

  // allows rendering of static files in this directory
  app.use(express.static(path.join(__dirname, PUBLIC_DIRECTORY)));

  // adds helmet module to express server
  // Helmet helps you secure your Express apps by setting various HTTP headers.
  // It's not a silver bullet, but it can help!
  app.use(
    helmet({
      contentSecurityPolicy: process.env.NODE_ENV === 'production',
    }),
  );

  // restricts the size of json body to 200kb
  app.use(express.json({ limit: MAX_REQ_BODY_SIZE }));
  app.use(express.urlencoded({ extended: true }));

  // logs request to console
  console.log('Setting up request logging...');
  app.use(morgan('tiny'));

  setV1Routes(app);
}

export const initServer = (app, appConfig) => {
  console.log('Starting server....');
  _configureServer(app);

  app.listen(appConfig.app.port, (err) => {
    if (err) {
      // server run failed
      console.log(`Failed to listen on port ${appConfig.app.port}`);
      console.error(err);
      process.exit(1);
    } else {
      // server run success
      console.log(`Listening on port ${appConfig.app.port}`);
      _printEnvDetails(appConfig);
    }
  });
};
