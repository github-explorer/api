require('dotenv').config();

const PROD_KEY = 'production';
const DEV_KEY = 'development';
const TEST_KEY = 'test';

const EXPRESS_CONFIG = Object.freeze({
  MAX_REQ_BODY_SIZE: '200kb',
  PUBLIC_DIRECTORY: '../public',
});

// I like separating envs
const development = {
  name: DEV_KEY,
  app: {
    port: process.env.PORT || 9000,
  },
};

const test = {
  name: TEST_KEY,
  app: {
    port: process.env.PORT || 9000,
  },
};

const production = {
  name: PROD_KEY,
  app: {
    port: process.env.PORT || 9000,
  },
};

const config = {
  development,
  production,
  test,
};

function _getEnvironment() {
  const tEnv = process.env.NODE_ENV;
  if (tEnv === PROD_KEY || tEnv === DEV_KEY || tEnv === TEST_KEY) return tEnv;
  // eslint-disable-next-line no-console
  console.log(
    '[Warning]: Invalid environment config in .env for key NODE_ENV\nPicking dev environment for running server',
  );
  return DEV_KEY;
}

const env = _getEnvironment();

// this method performs basic checks to check if environment config is valid
function checkIfEnvIsValid(appEnv) {
  if (!appEnv) {
    return false;
  }

  return true;
}

module.exports = config[env] || config[DEV_KEY];
module.exports.checkIfEnvIsValid = checkIfEnvIsValid;
module.exports.EXPRESS_CONFIG = EXPRESS_CONFIG;
