FROM node:14.17.6

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

COPY . .

EXPOSE 9000

CMD ['yarn' 'dev']
